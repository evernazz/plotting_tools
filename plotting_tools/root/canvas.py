from analysis_tools.utils import import_root
ROOT = import_root()

class Canvas(object):
    def __init__(self, canvas_name="", size_x=800, size_y=640, *args, **kwargs):
        self.canvas = ROOT.TCanvas(canvas_name, canvas_name, size_x, size_y)

    def cd(self):
        self.canvas.cd()

    def SaveAs(self, path):
        self.cd()
        self.canvas.SaveAs(path)

    def __getattr__(self, name):
        def fn(self=self, **kwargs):
            eval("self.canvas.%s()" % name)
            return True
        return fn


class DividedCanvas(Canvas):
    def __init__(self, canvas_name="", size_x=800, size_y=640, x_pads=1, y_pads=1, *args, **kwargs):
        super(DividedCanvas, self).__init__(canvas_name, size_x, size_y, *args, **kwargs)
        self.canvas.Divide(x_pads, y_pads)

    def get_pad(self, npad):
        self.pads = self.canvas.GetListOfPrimitives()
        return self.pads[npad - 1]


class RatioCanvas(DividedCanvas):
    def __init__(self, canvas_name="", size_x=800, size_y=640, *args, **kwargs):
        super(RatioCanvas, self).__init__(canvas_name, size_x, size_y, 1, 2, *args, **kwargs)
        self.canvas.SetWindowSize(size_x, size_y)
        self.canvas.SetCanvasSize(size_x, size_y)
        
        pad1 = self.get_pad(1)
        pad1.SetPad(0, 0.3, 1, 1)
        #pad1.SetPad(0, 0, 1, 1)
        pad2 = self.get_pad(2)
        pad2.SetPad(0, 0, 1, 0.3)
        #pad2.SetPad(0, 0, 1, 1)

        pad1.SetBottomMargin(0.)
        #pad1.SetBottomMargin(0)
        pad1.SetTopMargin(0.105)
        #pad1.Draw()
        #pad1.SetTopMargin(0.13)
        
        pad2.SetLogy(0)
        #pad2.SetBottomMargin(1)
        pad2.SetBottomMargin(0.3)
        pad2.SetTopMargin(0)
        #pad2.Draw("same")
        self.canvas.Update()

    def ratio_plot_style(self, dummy_histo_top, dummy_histo_bottom, min_y=None, max_y=None,
            min_y2=0.25, max_y2=1.75):
        if min_y:
            dummy_histo_top.SetMinimum(min_y)
        if max_y:
            dummy_histo_top.SetMaximum(max_y)
        dummy_histo_top.GetXaxis().SetTitleOffset(100)  # to effectively make it disappear
        dummy_histo_top.GetXaxis().SetLabelOffset(100)  # to effectively make it disappear
        dummy_histo_top.GetXaxis().SetLabelFont(43)
        dummy_histo_top.GetXaxis().SetLabelSize(15)
        dummy_histo_top.GetXaxis().SetTitleFont(43)
        dummy_histo_top.GetXaxis().SetTitleSize(20)
        dummy_histo_top.GetXaxis().SetTickLength(0.03)
        dummy_histo_top.GetYaxis().SetLabelFont(43)
        dummy_histo_top.GetYaxis().SetLabelSize(15)
        dummy_histo_top.GetYaxis().SetTitleFont(43)
        dummy_histo_top.GetYaxis().SetTitleSize(20)

        dummy_histo_bottom.SetMinimum(min_y2)
        dummy_histo_bottom.SetMaximum(max_y2)
        dummy_histo_bottom.GetXaxis().SetLabelFont(43)
        dummy_histo_bottom.GetXaxis().SetLabelSize(15)
        dummy_histo_bottom.GetXaxis().SetTitleFont(43)
        dummy_histo_bottom.GetXaxis().SetTitleSize(20)
        dummy_histo_bottom.GetYaxis().SetLabelFont(43)
        dummy_histo_bottom.GetYaxis().SetLabelSize(15)
        dummy_histo_bottom.GetYaxis().SetTitleFont(43)
        dummy_histo_bottom.GetYaxis().SetTitleSize(20)
        dummy_histo_bottom.GetXaxis().SetTitleOffset(3)
        dummy_histo_bottom.GetXaxis().SetTickLength(0.03 * 0.7 / 0.3)
        dummy_histo_bottom.GetYaxis().SetNdivisions(3)
        dummy_histo_bottom.GetYaxis().CenterTitle()
