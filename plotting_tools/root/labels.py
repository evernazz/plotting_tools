from analysis_tools.utils import import_root
ROOT = import_root()

def get_cms_label(upper_left="Private work", max=None, **kwargs):
    leftshift = 0
    if max != None: 
        if 1.25*max > 10000: leftshift = 0.05
    label = ROOT.TLatex(0.11+leftshift, 0.91, "#scale[1.5]{CMS} " + "{}".format(upper_left))
    label.SetNDC(True)
    scaling = kwargs.get("scaling", 1.)
    label.SetTextSize(0.04 * scaling)
    return label

def get_labels(upper_left="Private work", upper_right="2018 Simulation (13 TeV)", inner_text = None, max = None, **kwargs):
    labels = [get_cms_label(upper_left, max=max, **kwargs)]
    scaling = kwargs.get("scaling", 1.)
    if upper_right:
        label = ROOT.TLatex(0.90, 0.91, "{}".format(upper_right))
        label.SetNDC(True)
        label.SetTextSize(0.04 * scaling)
        label.SetTextAlign(31)
        labels.append(label)
    if inner_text:
        y = 0.85
        for text in inner_text:
            label = ROOT.TLatex(0.13, y, "{}".format(text))
            label.SetNDC(True)
            label.SetTextSize(0.03 * scaling)
            labels.append(label)
            y -= 0.05
    return labels
