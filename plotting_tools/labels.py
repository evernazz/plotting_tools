class Label():
    def __init__(self, latex=None, root=None, default="root"):
        assert (latex and not root) or (not latex and root)
        assert default in ["root", "latex"]
        if root:
            self.root = root
            self.latex = self.get_latex()
        elif latex:
            self.latex = r"%s" % latex
            self.root = self.get_root()
        self.default = default

    def get_latex(self):
        words = self.root.split(" ")
        for i, word in enumerate(words):
            if "#" in word:
                while "#" in word:
                    word = word[:word.find("#")] + "\\" + word[word.find("#") + 1:]
                words[i] = "$%s$" % word
        return " ".join(words)

    def get_root(self):
        words = self.latex.split(" ")
        for i, word in enumerate(words):
            if "$" in word:
                last_elem = -1
                if "," in word:
                    last_elem = -2
                assert word[0] == "$" and word[last_elem] == "$"
                while "\\" in word:
                    word = word[:word.find("\\")] + "#" + word[word.find("\\") + 1:]
                words[i] = word[1:-1]
        return " ".join(words)

    def __str__(self):
        return (self.root if self.default == "root" else self.latex)
